package nullgameengine.mesh;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.lwjgl.opengl.ARBVertexArrayObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

import de.javagl.obj.FloatTuple;
import de.javagl.obj.Obj;
import de.javagl.obj.ObjFace;
import de.javagl.obj.ObjReader;
import de.javagl.obj.ObjUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Mesh {
	private int vao;
	private int vbo;
	private int count;
	
	private void createGLObjects() {
		vao = ARBVertexArrayObject.glGenVertexArrays();
		vbo = GL15.glGenBuffers();
	}
	
	public static void unbind() {
		ARBVertexArrayObject.glBindVertexArray(0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}
	
	public void bind() {
		ARBVertexArrayObject.glBindVertexArray(vao);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
	}
	
	public void draw() {
		bind();
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, count);
		unbind();
	}
	
	public static Mesh loadMesh(InputStream is) throws IOException {
		Mesh outMesh = new Mesh();
		outMesh.createGLObjects();
		
		Obj obj = ObjReader.read(is);
		Obj triangulated = ObjUtils.triangulate(obj);
		
		boolean hasTexCoords = triangulated.getFace(0).containsTexCoordIndices();
		boolean hasNormals = triangulated.getFace(0).containsNormalIndices();
		
		int stride = 12;
		if(hasTexCoords) {
			stride += 8;
		}
		if(hasNormals) {
			stride += 12;
		}
		
		int count = triangulated.getNumFaces() * 3;
		ByteBuffer bb = ByteBuffer.allocateDirect(count * stride);
		
		bb.order(ByteOrder.nativeOrder());
		
		for(int i = 0; i < triangulated.getNumFaces(); i++) {
			ObjFace face = triangulated.getFace(i);
			for(int j = 0; j < 3; j++) {
				FloatTuple vert = triangulated.getVertex(face.getVertexIndex(j));
				bb.putFloat(vert.getX());
				bb.putFloat(vert.getY());
				bb.putFloat(vert.getZ());
				assert hasTexCoords == face.containsTexCoordIndices() : "Mesh.loadMesh: neither all nor none faces have texcoords";
				if(face.containsTexCoordIndices()) {
					FloatTuple uv = triangulated.getTexCoord(face.getTexCoordIndex(j));
					bb.putFloat(uv.getX());
					bb.putFloat(uv.getY());
				}
				assert hasNormals == face.containsNormalIndices() : "Mesh.loadMesh: neither all nor none faces have normals";
				if(face.containsNormalIndices()) {
					FloatTuple normal = triangulated.getNormal(face.getNormalIndex(j));
					bb.putFloat(normal.getX());
					bb.putFloat(normal.getY());
					bb.putFloat(normal.getZ());
				}
			}
		}
		
		bb.flip();

		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, outMesh.vbo);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, bb, GL15.GL_STATIC_DRAW);
		
		ARBVertexArrayObject.glBindVertexArray(outMesh.vao);

		GL20.glEnableVertexAttribArray(0);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, stride, 0);
		
		int offset = 12;
		if(hasTexCoords) {
			GL20.glEnableVertexAttribArray(1);
			GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, stride, offset);
			offset += 8;
		}
		if(hasNormals) {
			GL20.glEnableVertexAttribArray(2);
			GL20.glVertexAttribPointer(2, 3, GL11.GL_FLOAT, false, stride, offset);
			offset += 12;
		}
		
		outMesh.count = count;
		
		unbind();
		
		return outMesh;
	}
}
