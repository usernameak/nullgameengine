package nullgameengine.mesh.physical;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;

public class PhysicalBoxMesh implements IPhysicalMesh {
	private BoxShape shape; 
	
	public PhysicalBoxMesh(float w, float h, float d) {
		shape = new BoxShape(new Vector3f(w, h, d));
	}

	@Override
	public CollisionShape getBulletShape() {
		return shape;
	}

}
