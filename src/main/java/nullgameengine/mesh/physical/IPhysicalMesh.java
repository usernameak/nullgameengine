package nullgameengine.mesh.physical;

import com.bulletphysics.collision.shapes.CollisionShape;

public interface IPhysicalMesh {
	public CollisionShape getBulletShape();
}
