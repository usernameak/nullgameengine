package nullgameengine;

import java.io.IOException;
import java.io.InputStream;

import javax.vecmath.Vector3f;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.PixelFormat;

import lombok.Cleanup;
import nullgameengine.material.SimpleTexturedMaterial;
import nullgameengine.mesh.Mesh;
import nullgameengine.mesh.physical.PhysicalBoxMesh;
import nullgameengine.physics.DefaultScenePhysics;
import nullgameengine.physics.RigidBodyPhysics;
import nullgameengine.scene.CameraSceneObject;
import nullgameengine.scene.MeshSceneObject;
import nullgameengine.scene.Scene;
import nullgameengine.texture.Texture2D;

public class NullGameEngineTest {
	public static void main(String[] args) throws LWJGLException, IOException {
		Display.setResizable(true);
		Display.create(new PixelFormat(8, 16, 8, 8));
		
		Scene scene = new Scene();
		
		scene.initRenderer();
		
		DefaultScenePhysics physics = new DefaultScenePhysics(scene);
		physics.setGravity(-0.981f);
		scene.setPhysics(physics);
		
		@Cleanup InputStream is = NullGameEngineTest.class.getResourceAsStream("/assets/models/test.obj");
		Mesh mesh = Mesh.loadMesh(is);
		SimpleTexturedMaterial material = new SimpleTexturedMaterial();
		@Cleanup InputStream tis = NullGameEngineTest.class.getResourceAsStream("/assets/textures/test.png");
		Texture2D tex = Texture2D.loadTexture(tis);
		material.setTexture(tex);
		MeshSceneObject mso = new MeshSceneObject(mesh, material);
		mso.setPhysics(new RigidBodyPhysics(0f, new PhysicalBoxMesh(8, 8, 8)));
		mso.setPosition(new Vector3f(0, 0, 0));
		scene.addObject(mso);
		
		CameraSceneObject camera = new CameraSceneObject();
		camera.setPhysics(new RigidBodyPhysics(1f, new PhysicalBoxMesh(1, 1, 1)));
		camera.setPosition(new Vector3f(0, 0, 12));
		scene.addObject(camera);
		scene.setCamera(camera);
		
		while(!Display.isCloseRequested()) {
			scene.doRenderingAndStuff();
		}
	}
}
