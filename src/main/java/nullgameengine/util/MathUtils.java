package nullgameengine.util;

import org.joml.Matrix4f;

import com.bulletphysics.linearmath.Transform;

public class MathUtils {
	public static final Matrix4f matrixFromPhysicsTransform(Transform transform) {
		javax.vecmath.Matrix4f mat4v = transform.getMatrix(new javax.vecmath.Matrix4f());
		Matrix4f mat4 = new Matrix4f(mat4v.m00, mat4v.m10, mat4v.m20, mat4v.m30, mat4v.m01, mat4v.m11, mat4v.m21,
				mat4v.m31, mat4v.m02, mat4v.m12, mat4v.m22, mat4v.m32, mat4v.m03, mat4v.m13, mat4v.m23, mat4v.m33);
		return mat4;
	}
}
