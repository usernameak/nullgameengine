package nullgameengine.material;

import nullgameengine.scene.ICamera;
import nullgameengine.shader.ShaderProgram;

public interface IMaterial {
	public ShaderProgram getShader();
	public void bindMaterial();
	public void unbindMaterial();
	public void updateCamera(ICamera camera);
}
