package nullgameengine.material;

import lombok.Getter;
import lombok.Setter;
import nullgameengine.scene.ICamera;
import nullgameengine.shader.ShaderProgram;
import nullgameengine.shader.ShaderRegistry;
import nullgameengine.texture.Texture2D;

public class SimpleTexturedMaterial implements IMaterial {
	private static ShaderProgram shader = null;

	@Setter
	@Getter
	private Texture2D texture = null;

	@Override
	public ShaderProgram getShader() {
		if (shader == null) {
			shader = ShaderRegistry
					.getProgram("/assets/shaders/vertex_default.vp", "/assets/shaders/fragment_simple_textured.fp");
		}
		return shader;
	}
	
	@Override
	public void updateCamera(ICamera camera) {
		camera.getGlobalMatrices().bind(1);
		camera.getGlobalMatrices().bindToShader(getShader(), "globalMatrices");
	}

	public void bindMaterial() {
		getShader().use();
		texture.bind(0);
		getShader().setInteger("tex", 0);
	}

	public void unbindMaterial() {
		ShaderProgram.unuse();
		Texture2D.unbind(0);
	}
}
