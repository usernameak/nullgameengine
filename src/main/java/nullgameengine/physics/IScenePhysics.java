package nullgameengine.physics;

import javax.vecmath.Vector3f;

public interface IScenePhysics {
	public void addPhysicsObject(IPhysicsObject object);
	public void removePhysicsObject(IPhysicsObject object);
	public void doSimulation(float f);
}
