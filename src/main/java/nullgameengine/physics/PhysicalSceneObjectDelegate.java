package nullgameengine.physics;

import javax.vecmath.Vector3f;

import com.bulletphysics.linearmath.Transform;

import nullgameengine.scene.IPhysicalSceneObject;
import nullgameengine.util.NonNull;

public class PhysicalSceneObjectDelegate implements IPhysicalSceneObject {
	@NonNull
	private IPhysicsObject physicsObject = new DummyPhysicsObject();

	@Override
	public IPhysicsObject getPhysics() {
		return physicsObject;
	}

	@Override
	public void setPhysics(IPhysicsObject val) {
		physicsObject = val;
	}
	

	@Override
	public Transform getWorldTransform() {
		return physicsObject.getMotionState().getWorldTransform(new Transform());
	}
	
	@Override
	public void setWorldTransform(Transform transform) {
		physicsObject.updateTransform(transform);
	}

	@Override
	public Vector3f getPosition() {
		Vector3f origin = getWorldTransform().origin;
		return origin;
	}

	@Override
	public void setPosition(Vector3f pos) {
		Transform wt = getWorldTransform();
		wt.origin.set(pos);
		setWorldTransform(wt);
	}

}
