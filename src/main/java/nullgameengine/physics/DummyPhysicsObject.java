package nullgameengine.physics;

import javax.vecmath.Vector3f;

import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

public class DummyPhysicsObject implements IPhysicsObject {
	private DefaultMotionState dms = new DefaultMotionState();
	
	@Override
	public Object getBulletObject() {
		return null;
	}
	
	public MotionState getMotionState() {
		return dms;
	}

	@Override
	public void updateTransform(Transform transform) {
		dms.setWorldTransform(transform);
	}

	@Override
	public void doForce(Vector3f accel) {
	}
	
}
