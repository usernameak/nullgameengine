package nullgameengine.physics;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nullgameengine.scene.Scene;
import nullgameengine.util.NonNull;

@RequiredArgsConstructor
public class DefaultScenePhysics implements IScenePhysics {
	@Getter
	@NonNull
	private final Scene scene;
	
	private CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();
	private CollisionDispatcher collisionDispatcher = new CollisionDispatcher(collisionConfiguration);
	private BroadphaseInterface broadphase = new DbvtBroadphase();
	private ConstraintSolver solver = new SequentialImpulseConstraintSolver();
	private DiscreteDynamicsWorld world = new DiscreteDynamicsWorld(collisionDispatcher, broadphase, solver, collisionConfiguration);
	
	public void setGravity(float x, float y, float z) {
		world.setGravity(new Vector3f(x, y, z));
	}
	
	public void setGravity(float z) {
		setGravity(0f, 0f, z);
	}
	
	@Override
	public void addPhysicsObject(IPhysicsObject object) {
		Object bulletObject = object.getBulletObject();
		if(bulletObject == null) {
			// Dummy object
		} else if(bulletObject instanceof RigidBody) {
			world.addRigidBody((RigidBody) bulletObject);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public void removePhysicsObject(IPhysicsObject object) {
		Object bulletObject = object.getBulletObject();
		if(bulletObject == null) {
			// Dummy object
		} else if(bulletObject instanceof RigidBody) {
			world.removeRigidBody((RigidBody) bulletObject);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public void doSimulation(float time) {
		world.stepSimulation(time);
	}
}
