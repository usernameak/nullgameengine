package nullgameengine.physics;

import javax.vecmath.Vector3f;

import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

public interface IPhysicsObject {
	public Object getBulletObject();
	public MotionState getMotionState();
	public void updateTransform(Transform transform);
	void doForce(Vector3f accel);
}
