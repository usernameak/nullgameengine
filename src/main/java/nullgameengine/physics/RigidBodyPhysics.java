package nullgameengine.physics;

import javax.vecmath.Vector3f;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;

import nullgameengine.mesh.physical.IPhysicalMesh;

public class RigidBodyPhysics implements IPhysicsObject {
	private RigidBody bulletObject;
	
	public RigidBodyPhysics(float mass, IPhysicalMesh mesh) {
		bulletObject = new RigidBody(mass, new DefaultMotionState(), mesh.getBulletShape());
		setFriction(0.45f);
	}
	
	@Override
	public Object getBulletObject() {
		return bulletObject;
	}

	@Override
	public MotionState getMotionState() {
		return bulletObject.getMotionState();
	}
	
	public void setFriction(float friction) {
		bulletObject.setFriction(friction);
	}

	@Override
	public void updateTransform(Transform transform) {
		bulletObject.proceedToTransform(transform);
		bulletObject.activate(true);
	}
	
	public Transform getWorldTransform() {
		return getMotionState().getWorldTransform(new Transform());
	}
	
	public void setWorldTransform(Transform transform) {
		updateTransform(transform);
	}

	public Vector3f getPosition() {
		Vector3f origin = getWorldTransform().origin;
		return origin;
	}

	public void setPosition(Vector3f pos) {
		Transform wt = getWorldTransform();
		wt.origin.set(pos);
		setWorldTransform(wt);
	}

	@Override
	public void doForce(Vector3f force) {
		bulletObject.applyForce(force, getPosition());
		bulletObject.activate(true);
	}

}
