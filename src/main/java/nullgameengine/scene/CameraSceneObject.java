package nullgameengine.scene;

import javax.vecmath.Vector3f;

import org.joml.AxisAngle4f;
import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector4f;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.bulletphysics.linearmath.Transform;

import lombok.experimental.Delegate;
import nullgameengine.physics.PhysicalSceneObjectDelegate;
import nullgameengine.shader.UniformBuffer;
import nullgameengine.util.MathUtils;

public class CameraSceneObject implements ICamera {
	private UniformBuffer globalMatrices;

	@Delegate
	private PhysicalSceneObjectDelegate physicsDelegate = new PhysicalSceneObjectDelegate();
	
	private float rotationX = 0.f;
	private float rotationY = 0.f;

	@Override
	public void initRenderer() {
		globalMatrices = UniformBuffer.builder().addMat4("projectionMatrix").addMat4("viewMatrix").build();
		applyMatrices();
	}
	
	private void applyMatrices() {
		Transform transform = getWorldTransform();
		globalMatrices
				.setMat4("projectionMatrix", new Matrix4f()
						.perspective(70f, (float) Display.getWidth() / (float) Display.getHeight(), 0.001f, 10000.f));
		globalMatrices
				.setMat4("viewMatrix",
						new Matrix4f()
								.lookAt(0, 0, 0, 0, 1, 0, 0f, 0f, 1f).rotate(-rotationY, 1, 0, 0).rotate(rotationX, 0, 0, 1)
								.mul(MathUtils.matrixFromPhysicsTransform(transform).invert()));
	}

	@Override
	public void render(Scene scene, ICamera camera) {
		applyMatrices();
		
		for (ISceneObject obj : scene.getObjects()) {
			if ((obj.getFlags() & FLAG_IS_CAMERA) != 0 || obj == this) {
				continue;
			}
			obj.render(scene, this);
		}
	}

	@Override
	public long getFlags() {
		return FLAG_IS_CAMERA;
	}

	@Override
	public UniformBuffer getGlobalMatrices() {
		return globalMatrices;
	}
	
	private Vector3f applyRotation(Vector3f vec) {
		Vector4f v4 = new Matrix4f().rotate(rotationY, 1, 0, 0).rotate(rotationX, 0, 0, 1).transform(new Vector4f(-vec.x, vec.y, vec.z, 1.f));
		return new Vector3f(- v4.x / v4.w, v4.y / v4.w, v4.z / v4.w);
	}
	
	private Vector3f applyRotationX(Vector3f vec) {
		Vector4f v4 = new Matrix4f().rotate(rotationX, 0, 0, 1).transform(new Vector4f(-vec.x, vec.y, vec.z, 1.f));
		return new Vector3f(- v4.x / v4.w, v4.y / v4.w, v4.z / v4.w);
	}

	@Override
	public void update(Scene scene, float delta) {
		rotationX += Mouse.getDX() * 0.001f;
		rotationY += Mouse.getDY() * 0.001f;
		if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
			getPhysics().doForce(applyRotationX(new Vector3f(0f, 1f, 0.f)));
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
			getPhysics().doForce(applyRotationX(new Vector3f(0f, -1f, 0.f)));
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
			getPhysics().doForce(applyRotationX(new Vector3f(-1f, 0f, 0.f)));
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
			getPhysics().doForce(applyRotationX(new Vector3f(1f, 0f, 0.f)));
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			getPhysics().doForce(new Vector3f(0f, 0f, 1.f));
		}
		
	}

}
