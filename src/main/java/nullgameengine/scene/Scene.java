package nullgameengine.scene;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import lombok.Getter;
import nullgameengine.physics.IScenePhysics;

public class Scene {
	private Set<ISceneObject> objects = new HashSet<>();

	private ICamera cameraObject;

	@Getter
	private IScenePhysics physics = null;

	public void initRenderer() {
		Mouse.setGrabbed(true);
	}

	public void setPhysics(IScenePhysics physics) {
		if (this.physics != null) {
			for (ISceneObject object : objects) {
				if (object.getPhysics() != null) {
					this.physics.removePhysicsObject(object.getPhysics());
				}
			}
		}
		this.physics = physics;

		for (ISceneObject object : objects) {
			if (object.getPhysics() != null) {
				this.physics.addPhysicsObject(object.getPhysics());
			}
		}
	}

	public Set<ISceneObject> getObjects() {
		return Collections.unmodifiableSet(objects);
	}

	public void addObject(ISceneObject obj) {
		objects.add(obj);
		obj.initRenderer();
		if (physics != null) {
			this.physics.addPhysicsObject(obj.getPhysics());
		}
	}

	public void setCamera(ICamera obj) {
		assert objects.contains(obj);
		cameraObject = obj;
	}
	
	private long timer = System.currentTimeMillis();
	private long delta = 0;

	private float getDelta() {
		return delta / 1000.f;
	}

	public void doRenderingAndStuff() {
		long prevTimer = timer;
		
		GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());

		GL11.glClearColor(0.0f, 0.0f, 0.0f, 1.f);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		GL11.glEnable(GL11.GL_DEPTH_TEST);

		render();

		Display.update();
		
		if (physics != null) {
			physics.doSimulation(getDelta());
		}

		for (ISceneObject object : objects) {
			object.update(this, getDelta());
		}
		
		timer = System.currentTimeMillis();
		delta = timer - prevTimer;
	}

	public void render() {
		cameraObject.render(this, cameraObject);
	}
}
