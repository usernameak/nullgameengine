package nullgameengine.scene;

import org.joml.Matrix4f;

import lombok.experimental.Delegate;
import nullgameengine.material.IMaterial;
import nullgameengine.mesh.Mesh;
import nullgameengine.physics.PhysicalSceneObjectDelegate;
import nullgameengine.shader.UniformBuffer;
import nullgameengine.util.MathUtils;

public class MeshSceneObject implements ISceneObject {
	private Mesh mesh;
	private IMaterial material;
	private UniformBuffer meshMatrices;
	
	@Delegate
	private PhysicalSceneObjectDelegate physicsDelegate = new PhysicalSceneObjectDelegate();
	
	public MeshSceneObject(Mesh mesh, IMaterial material) {
		this.mesh = mesh;
		this.material = material;
	}
	
	/**
	 * Renders this object.
	 * @param scene Scene the object belongs to
	 * @param camera Camera which is used for rendering
	 */
	@Override
	public void render(Scene scene, ICamera camera) {
		material.updateCamera(camera);
		material.bindMaterial();
		meshMatrices.bind(2);
		meshMatrices.setMat4("modelMatrix", MathUtils.matrixFromPhysicsTransform(getWorldTransform()));
		meshMatrices.bindToShader(material.getShader(), "meshMatrices");
		mesh.draw();
		material.unbindMaterial();
	}

	@Override
	public long getFlags() {
		return 0;
	}

	@Override
	public void initRenderer() {
		meshMatrices = UniformBuffer.builder().addMat4("modelMatrix").build();
		meshMatrices.setMat4("modelMatrix", MathUtils.matrixFromPhysicsTransform(getWorldTransform()));
	}

	@Override
	public void update(Scene scene, float delta) {
		
	}

}
