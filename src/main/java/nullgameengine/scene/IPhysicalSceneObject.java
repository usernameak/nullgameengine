package nullgameengine.scene;

import javax.vecmath.Vector3f;

import com.bulletphysics.linearmath.Transform;

import nullgameengine.physics.IPhysicsObject;

public interface IPhysicalSceneObject {
	public IPhysicsObject getPhysics();

	public void setPhysics(IPhysicsObject val);
	
	public Vector3f getPosition();
	
	public void setPosition(Vector3f pos);

	Transform getWorldTransform();

	void setWorldTransform(Transform transform);
}
