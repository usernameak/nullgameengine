package nullgameengine.scene;

public interface ISceneObject extends IPhysicalSceneObject {
	public static final long FLAG_IS_CAMERA = 1L;

	/**
	 * Renders this object.
	 * @param scene Scene the object belongs to
	 * @param camera Camera which is used for rendering
	 */
	public void render(Scene scene, ICamera camera);
	
	public long getFlags();

	public void initRenderer();
	
	public void update(Scene scene, float delta);
}
