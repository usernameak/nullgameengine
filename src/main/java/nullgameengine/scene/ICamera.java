package nullgameengine.scene;

import nullgameengine.shader.UniformBuffer;

public interface ICamera extends ISceneObject {
	public UniformBuffer getGlobalMatrices();
}
