package nullgameengine.texture;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

public class Texture2D {
	private int tex;
	
	private Texture2D() {
		
	}
	
	private void createGLObjects() {
		tex = GL11.glGenTextures();
	}
	

	public void bind(int target) {
		GL13.glActiveTexture(target);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
	}
	
	public void bind() {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
	}
	
	public static void unbind() {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
	public static void unbind(int target) {
		GL13.glActiveTexture(target);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
	public static Texture2D loadTexture(InputStream is) throws IOException {
		BufferedImage image = ImageIO.read(is);
		int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

        ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);
        
        for(int y = 0; y < image.getHeight(); y++){
            for(int x = 0; x < image.getWidth(); x++){
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));
                buffer.put((byte) ((pixel >> 8) & 0xFF));
                buffer.put((byte) (pixel & 0xFF));
                buffer.put((byte) ((pixel >> 24) & 0xFF));
            }
        }

        buffer.flip();
        
        Texture2D tex = new Texture2D();
        
        tex.createGLObjects();
        
        tex.bind();
        
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, image.getWidth(), image.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);

        unbind();
        
        return tex;
	}
}
