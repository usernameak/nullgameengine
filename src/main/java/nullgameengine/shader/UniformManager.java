package nullgameengine.shader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import org.joml.Matrix4f;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

public abstract class UniformManager {
	@AllArgsConstructor(access = AccessLevel.PROTECTED)
	public static enum UniformElementType {
		MAT4(4 * 4 * 4), INTEGER(4);

		public final int size;
	}

	@AllArgsConstructor(access = AccessLevel.PROTECTED)
	protected static class UniformInfo<T> {
		public UniformInfo(String name, UniformElementType type) {
			super();
			this.name = name;
			this.type = type;
		}

		public String name;
		public UniformElementType type;
		public int offset = -1;
		public T value;
	}

	protected abstract <T> UniformInfo<T> findUniformInfo(String name, UniformElementType type);

	protected abstract void putData(int offset, ByteBuffer data, UniformInfo<?> info);

	public void setMat4(String name, Matrix4f matrix) {
		UniformInfo<Matrix4f> info = findUniformInfo(name, UniformElementType.MAT4);

		ByteBuffer bb = ByteBuffer.allocateDirect(UniformElementType.MAT4.size).order(ByteOrder.nativeOrder());
		FloatBuffer fb = bb.asFloatBuffer();
		matrix.get(fb);
		putData(info.offset, bb, info);

		info.value = matrix;
	}

	public Matrix4f getMat4(String name) {
		UniformInfo<Matrix4f> info = findUniformInfo(name, UniformElementType.MAT4);

		return info.value;
	}
	
	public void setInteger(String name, int value) {
		UniformInfo<Integer> info = findUniformInfo(name, UniformElementType.INTEGER);

		ByteBuffer bb = ByteBuffer.allocateDirect(UniformElementType.INTEGER.size).order(ByteOrder.nativeOrder());
		bb.putInt(value);
		putData(info.offset, bb, info);

		info.value = value;
	}

	public int getInteger(String name) {
		UniformInfo<Integer> info = findUniformInfo(name, UniformElementType.INTEGER);

		return info.value;
	}
}
