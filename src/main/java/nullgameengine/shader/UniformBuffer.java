package nullgameengine.shader;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

import org.joml.Matrix4f;
import org.lwjgl.opengl.ARBUniformBufferObject;
import org.lwjgl.opengl.GL15;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

public class UniformBuffer extends UniformManager {

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class Builder {

		private final List<UniformInfo<?>> uniforms = new LinkedList<>();

		public Builder addMat4(String name) {
			uniforms.add(new UniformInfo<Matrix4f>(name, UniformElementType.MAT4));
			return this;
		}
		
		public Builder addInteger(String name) {
			uniforms.add(new UniformInfo<Integer>(name, UniformElementType.INTEGER));
			return this;
		}

		public UniformBuffer build() {
			UniformBuffer ubo = new UniformBuffer();

			ubo.uniforms.addAll(uniforms);

			ubo.allocate();

			return ubo;
		}
	}

	private int vbo;

	private final List<UniformInfo<?>> uniforms = new LinkedList<>();

	private int bindingPoint = -1;

	private void allocate() {
		createGLObjects();

		int size = 0;

		for (UniformInfo<?> info : uniforms) {
			info.offset = size;
			size += info.type.size;
		}

		GL15.glBindBuffer(ARBUniformBufferObject.GL_UNIFORM_BUFFER, vbo);
		GL15.glBufferData(ARBUniformBufferObject.GL_UNIFORM_BUFFER, size, GL15.GL_STREAM_DRAW);
		GL15.glBindBuffer(ARBUniformBufferObject.GL_UNIFORM_BUFFER, 0);
	}

	public void bind(int point) {
		bindingPoint = point;
		ARBUniformBufferObject.glBindBufferBase(ARBUniformBufferObject.GL_UNIFORM_BUFFER, point, vbo);
	}

	public void bindToShader(ShaderProgram program, String name) {
		ARBUniformBufferObject
				.glUniformBlockBinding(program.getIndex(),
						ARBUniformBufferObject.glGetUniformBlockIndex(program.getIndex(), name), bindingPoint);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> UniformInfo<T> findUniformInfo(String name, UniformElementType type) {
		for (UniformInfo<?> info : uniforms) {
			if (info.name == name) {
				if (info.type != type) {
					throw new IllegalArgumentException();
				}
				return (UniformInfo<T>) info;
			}
		}
		throw new IllegalArgumentException();
	}

	private void createGLObjects() {
		vbo = GL15.glGenBuffers();
	}

	public static Builder builder() {
		return new Builder();
	}

	@Override
	protected void putData(int offset, ByteBuffer data, UniformInfo<?> info) {
		GL15.glBindBuffer(ARBUniformBufferObject.GL_UNIFORM_BUFFER, vbo);
		GL15.glBufferSubData(ARBUniformBufferObject.GL_UNIFORM_BUFFER, info.offset, data);
		GL15.glBindBuffer(ARBUniformBufferObject.GL_UNIFORM_BUFFER, 0);
		
	}
}
