package nullgameengine.shader;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.lwjgl.opengl.GL20;

import lombok.Cleanup;
import nullgameengine.NullGameEngineTest;

public final class ShaderRegistry {
	private static final Map<Pair<String, String>, ShaderProgram> registry = new HashMap<>();

	public static final ShaderProgram getProgram(String vertexShader, String fragmentShader) {
		Pair<String, String> pair = Pair.of(vertexShader, fragmentShader);
		if (!registry.containsKey(pair)) {
			try {
				@Cleanup
				InputStream vsis = NullGameEngineTest.class.getResourceAsStream(vertexShader);

				Shader vs = Shader.loadShader(vsis, GL20.GL_VERTEX_SHADER);

				@Cleanup
				InputStream fsis = NullGameEngineTest.class.getResourceAsStream(fragmentShader);

				Shader fs = Shader.loadShader(fsis, GL20.GL_FRAGMENT_SHADER);

				ShaderProgram program = ShaderProgram.createProgram(vs, fs);

				registry.put(pair, program);
				return program;
			} catch (IOException | ShaderCompileException | ShaderLinkException e) {
				throw new RuntimeException(e);
			}
		}
		return registry.get(pair);
	}
}
