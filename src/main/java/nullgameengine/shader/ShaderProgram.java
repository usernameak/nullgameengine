package nullgameengine.shader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ShaderProgram extends UniformManager {
	private int po;
	
	private Map<String, UniformInfo<?>> uniforms = new HashMap<>();
	
	public void use() {
		GL20.glUseProgram(po);
	}
	
	public static void unuse() {
		GL20.glUseProgram(0);
	}

	public static ShaderProgram createProgram(Shader vertex, Shader fragment) throws IOException, ShaderLinkException {
		ShaderProgram prog = new ShaderProgram();
		
		prog.createGLObjects();
		vertex.attach(prog.po);
		fragment.attach(prog.po);
		
		GL20.glBindAttribLocation(prog.po, 0, "position");
		GL20.glBindAttribLocation(prog.po, 1, "texCoord");
		GL20.glBindAttribLocation(prog.po, 2, "normal");
		
		GL20.glLinkProgram(prog.po);
		if(GL20.glGetProgrami(prog.po, GL20.GL_LINK_STATUS) != GL11.GL_TRUE) {
			throw new ShaderLinkException(GL20.glGetProgramInfoLog(prog.po, GL20.glGetProgrami(prog.po, GL20.GL_INFO_LOG_LENGTH)));
		}
		
		return prog;
	}
	
	public int getIndex() {
		return po;
	}

	private void createGLObjects() {
		po = GL20.glCreateProgram();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected <T> UniformInfo<T> findUniformInfo(String name, UniformElementType type) {
		if(uniforms.containsKey(name)) {
			return (UniformInfo<T>) uniforms.get(name);
		}
		UniformInfo<T> uniform = new UniformInfo<T>(name, type, GL20.glGetUniformLocation(po, name), null);
		uniforms.put(name, uniform);
		return uniform;
	}

	@Override
	protected void putData(int offset, ByteBuffer data, UniformInfo<?> info) {
		switch(info.type) {
		case MAT4:
			GL20.glUniformMatrix4(offset, false, data.asFloatBuffer());
			break;
		case INTEGER:
			GL20.glUniform1(offset, data.asIntBuffer());
			break;
		}
	}
}
