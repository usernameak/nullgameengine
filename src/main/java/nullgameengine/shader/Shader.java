package nullgameengine.shader;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Shader {
	private int so;

	private void createGLObjects(int type) {
		so = GL20.glCreateShader(type);
	}
	
	void attach(int program) {
		GL20.glAttachShader(program, so);
	}

	public static Shader loadShader(InputStream is, int type) throws IOException, ShaderCompileException {
		Shader shader = new Shader();
		shader.createGLObjects(type);
		GL20.glShaderSource(shader.so, IOUtils.toString(is, "UTF-8"));
		GL20.glCompileShader(shader.so);
		if(GL20.glGetShaderi(shader.so, GL20.GL_COMPILE_STATUS) != GL11.GL_TRUE) {
			throw new ShaderCompileException("Error compiling shader: " + GL20.glGetShaderInfoLog(shader.so, GL20.glGetShaderi(shader.so, GL20.GL_INFO_LOG_LENGTH)));
		}
		
		return shader;
	}
}
