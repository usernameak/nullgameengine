package nullgameengine.shader;

public class ShaderLinkException extends Exception {
	private static final long serialVersionUID = 1L;

	public ShaderLinkException() {
		super();
	}

	public ShaderLinkException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public ShaderLinkException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ShaderLinkException(String arg0) {
		super(arg0);
	}

	public ShaderLinkException(Throwable arg0) {
		super(arg0);
	}
}
